class ApplicationController < ActionController::API
  def students
    render json: {
      students: Student.all.map do |student|
        {
          name: student.name,
          classes: student.klasses.map(&:name)
        }
      end
    }
  end

  def stats
    render json: {
      students_count: Student.count,
      classes_count: Klass.count,
    }
  end
end
