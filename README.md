Welcome to the Classmates project.

Please make sure the project works for you locally before starting the assessment.

# Setup

```bash
$ bundle install
$ rails db:setup
$ rails s
$ open http://localhost:3000/students
```


# Send

Create a git patch file using:

	git format-patch master --stdout > test-task.patch
	
(replace `master` with you branch if relevant, and `test-task` with your name)
	
and send it to us at tech-interview@90seconds.tv
